import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import game.of.life.Cell;
import game.of.life.Cell.CellState;

class CellTest {

	@Test
	void testCellDiesWithLessThan2Neighbours() {
		Cell cell = new Cell(Cell.CellState.ALIVE);
		for (int i = 0; i < 2; i++) {
			CellState actualState = cell.getNextState(i);
			assertEquals(Cell.CellState.DEAD, actualState);		
		}
	}
	
	@Test
	void testCellLivesWith2Neighbours() {
		Cell cell = new Cell(Cell.CellState.ALIVE);
		CellState actualState = cell.getNextState(2);
		assertEquals(Cell.CellState.ALIVE, actualState);
	}
	
	@Test
	void testCellDiesWithMoreThan4Neighbours() {
		Cell cell = new Cell(Cell.CellState.ALIVE);
		CellState actualState = null;
		for (int i = 4; i <= 8; i++) {
			actualState = cell.getNextState(i);
		}
		assertEquals(Cell.CellState.DEAD, actualState);
	}
	@Test
	void testCellBecomeAliveWith3Neighbours() {
		Cell cell = new Cell(Cell.CellState.ALIVE);
		CellState actualState = cell.getNextState(3);
		assertEquals(Cell.CellState.ALIVE, actualState);
	}
}
